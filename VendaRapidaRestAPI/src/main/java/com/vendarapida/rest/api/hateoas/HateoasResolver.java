package com.vendarapida.rest.api.hateoas;

import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.vendarapida.rest.api.common.dto.AbstractDTO;
import com.vendarapida.rest.api.util.ReflectionUtil;

public abstract class HateoasResolver<Resource, DTO extends AbstractDTO> {
	
	public abstract void resolveHateoas(DTO dto, UriInfo uriInfo);
	
	public void buildHateoas(DTO dto, UriInfo uriInfo) {
		dto.clearLinks();
		resolveHateoas(dto, uriInfo);
	}
	
	public void buildHateoasLink(UriInfo uriInfo, DTO dto, String methodName, String rel) {
		
		Class<?> resourceClass = getResourceClass();
		
		String baseURI = uriInfo.getBaseUri().toString();
		MultivaluedMap<String,String> baseParams = uriInfo.getPathParameters();
		Map<String, Object> preparedParameters = prepareParameters(baseParams);
		
		Parameter hateoasParameterId = getHateoasParameterId(resourceClass, methodName);
		prepareHateoasParameterId(preparedParameters, resourceClass, methodName, hateoasParameterId, dto);
		
		UriBuilder uriBuilder = UriBuilder.fromPath(baseURI).path(resourceClass);
		uriBuilder = uriBuilder.path(resourceClass, methodName);
		uriBuilder = uriBuilder.resolveTemplates(preparedParameters);
		
        Link.Builder linkBuilder = Link.fromUriBuilder(uriBuilder).rel(rel);
        dto.addLink(linkBuilder.build());
	}
	
	private Class<?> getResourceClass() {
		try {
			ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
			Type resourceType = (parameterizedType).getActualTypeArguments()[0];
			String typeName = resourceType.getTypeName();
			return Class.forName(typeName);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private Map<String, Object> prepareParameters(MultivaluedMap<String, String> pathParameters) {
        Map<String, Object> preparedParameters = new HashMap<String, Object>();
        
        if (Objects.nonNull(pathParameters)) {
            Iterator<String> pathParametersIterator = pathParameters.keySet().iterator();
            
            while (pathParametersIterator.hasNext()) {
                String parameterName = (String) pathParametersIterator.next();
                preparedParameters.put(parameterName, pathParameters.getFirst(parameterName));
            }
        }
        return preparedParameters;
    }
	
	private void prepareHateoasParameterId(Map<String, Object> preparedParameters, Class<?> resourceClass, String methodName, Parameter hateoasParameterId, DTO dto) {
		
		if(preparedParameters.isEmpty() && Objects.nonNull(hateoasParameterId)) {
			String pathParamName = getPathParamName(resourceClass, methodName, hateoasParameterId);
			preparedParameters.put(pathParamName, getValidDTOId(dto));
		}
	}

	private Parameter getHateoasParameterId(Class<?> resourceClass, String methodName) {
		List<Parameter> hateoasParameters = ReflectionUtil.getParametersByMethod(resourceClass, methodName);
		
		if(hateoasParameters.size() > 1) {
			String resourceName = resourceClass.getName();
			throw new RuntimeException("The hateoas method " + methodName + " of " + resourceName + " must contain only one reference id parameter");
		}
		return hateoasParameters.stream().findFirst().orElse(null);
	}
	
	private String getPathParamName(Class<?> resourceClass, String methodName, Parameter parameter) {
		PathParam pathParam = parameter.getAnnotation(PathParam.class);
		if(Objects.isNull(pathParam)) {
			throw new  RuntimeException("PathParam annotation not found in hateoas method " + methodName + " on class " + resourceClass.getName());
		}
		return pathParam.value();
	}
	
	private Long getValidDTOId(DTO dto) {
		if(Objects.isNull(dto.getId())) {
			throw new RuntimeException("Id is mandatory to build hateoas for DTO " + dto.getClass().getSimpleName());
		}
		return dto.getId();
	}
}
