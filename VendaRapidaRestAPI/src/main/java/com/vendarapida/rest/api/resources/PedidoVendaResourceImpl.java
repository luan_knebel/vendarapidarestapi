package com.vendarapida.rest.api.resources;

import java.util.List;
import java.util.Objects;

import javax.annotation.ManagedBean;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.PedidoVendaDTO;
import com.vendarapida.rest.api.common.dto.UsuarioDTO;
import com.vendarapida.rest.api.common.resources.PedidoVendaResource;
import com.vendarapida.rest.api.jwt.ContextRequestCache;
import com.vendarapida.rest.api.jwt.LoginContext;
import com.vendarapida.rest.api.repository.PedidoVendaRepository;
import com.vendarapida.rest.api.repository.UsuarioRepository;

@ManagedBean
public class PedidoVendaResourceImpl extends AbstractResource implements PedidoVendaResource{

	@Inject
	private PedidoVendaRepository pedidoVendaRepository;
	
	@Inject
	private UsuarioRepository usuarioRepository;
	
	@Override
	public List<PedidoVendaDTO> getTodosRegistros() {
		return pedidoVendaRepository.getTodosRegistros();
	}
	
	@Override
	public PedidoVendaDTO buscarPorId(Long idPedidoVenda) {
		return pedidoVendaRepository.buscarPorId(idPedidoVenda);
	}
	
	@Override
	public Long cadastrar(PedidoVendaDTO pedidoVendaDTO){
		vincularUsuarioLogadoNoPedidoVenda(pedidoVendaDTO);
		return pedidoVendaRepository.cadastrar(pedidoVendaDTO);
	}
	
	@Override
	public void atualizar(Long idPedidoVenda, PedidoVendaDTO pedidoVendaDTO) {
		vincularUsuarioLogadoNoPedidoVenda(pedidoVendaDTO);
		pedidoVendaRepository.atualizar(idPedidoVenda, pedidoVendaDTO);
	}
	
	@Override
	public void excluir(Long idPedidoVenda) {
		pedidoVendaRepository.excluir(idPedidoVenda);
	}
	
	private void vincularUsuarioLogadoNoPedidoVenda(PedidoVendaDTO pedidoVendaDTO) {
		if(Objects.nonNull(pedidoVendaDTO)) {
			LoginContext loginContext = ContextRequestCache.get().getLoginContext();
			Long idUsuario = loginContext.getIdUsuario();
			UsuarioDTO usuarioDTO = usuarioRepository.buscarPorId(idUsuario);
			pedidoVendaDTO.setUsuario(usuarioDTO);
		}
	}

}
