package com.vendarapida.rest.api.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.ItemPedidoVendaDTO;
import com.vendarapida.rest.api.common.dto.PedidoVendaDTO;
import com.vendarapida.rest.api.common.dto.ProdutoDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;

@ManagedBean
@ApplicationScoped
public class ItemPedidoVendaRepository extends AbstractRepository<ItemPedidoVendaDTO>{

	@Inject
	private ProdutoRepository produtoRepository;
	
	@Inject
	private PedidoVendaRepository pedidoVendaRepository;
	
	public List<ItemPedidoVendaDTO> buscarPorIdPedidoVenda(Long idPedidoVenda) {
		PedidoVendaDTO pedidoVendaDTO = pedidoVendaRepository.buscarPorId(idPedidoVenda);
		return pedidoVendaDTO.getItens();
	}
	
	public ItemPedidoVendaDTO buscarPorIdItemPedidoVenda(Long idPedidoVenda, Long idItemPedidoVenda) {
		PedidoVendaDTO pedidoVendaDTO = pedidoVendaRepository.buscarPorId(idPedidoVenda);
		return buscarItemPedidoVendaPorId(pedidoVendaDTO, idItemPedidoVenda);
	}
	
	@Override
	public Long cadastrar(ItemPedidoVendaDTO itemPedidoVendaDTO) {
		throw new BusinessException("Utilize o método cadastrarItemPedidoVendaDTO");
	}

	@Override
	public void atualizar(Long id, ItemPedidoVendaDTO itemPedidoVendaDTO) {
		throw new BusinessException("Utilize o método atualizarItemPedidoVendaDTO");
	}
	
	@Override
	public void excluir(Long id) {
		throw new BusinessException("Utilize o método excluirItemPedidoVendaDTO");
	}
	
	public Long cadastrarItemPedidoVendaDTO(Long idPedidoVenda, ItemPedidoVendaDTO itemPedidoVendaDTO) {
		PedidoVendaDTO pedidoVendaDTO = pedidoVendaRepository.buscarPorId(idPedidoVenda);
		return cadastrarItemPedidoVendaDTO(pedidoVendaDTO, itemPedidoVendaDTO);
	}
	
	public Long cadastrarItemPedidoVendaDTO(PedidoVendaDTO pedidoVendaDTO, ItemPedidoVendaDTO itemPedidoVendaDTO) {
		validarCamposObrigatorios(itemPedidoVendaDTO);
		
		Long idProduto = itemPedidoVendaDTO.getProduto().getIdProduto();
		ProdutoDTO produtoDTO = produtoRepository.buscarPorId(idProduto);
		
		Long proximoId = getProximoId();
		itemPedidoVendaDTO.setProduto(produtoDTO);
		itemPedidoVendaDTO.setIdItemPedidoVenda(proximoId);
		
		adicionarRegistro(itemPedidoVendaDTO);
		pedidoVendaDTO.addItemPedidoVendaDTO(itemPedidoVendaDTO);
		calcularValorPedido(pedidoVendaDTO);
		return proximoId;
	}
	
	public void atualizarItemPedidoVendaDTO(Long idPedidoVenda, Long idItemPedidoVenda, ItemPedidoVendaDTO itemPedidoVendaDTO) {
		PedidoVendaDTO pedidoVendaDTO = pedidoVendaRepository.buscarPorId(idPedidoVenda);
		validarCamposObrigatorios(itemPedidoVendaDTO);
		
		Long idProduto = itemPedidoVendaDTO.getProduto().getIdProduto();
		ProdutoDTO produtoDTO = produtoRepository.buscarPorId(idProduto);
		
		ItemPedidoVendaDTO itemPedidoVendaAtualizarDTO = buscarItemPedidoVendaPorId(pedidoVendaDTO, idItemPedidoVenda);
		itemPedidoVendaAtualizarDTO.setProduto(produtoDTO);
		itemPedidoVendaAtualizarDTO.setValorProduto(itemPedidoVendaDTO.getValorProduto());
		
		calcularValorPedido(pedidoVendaDTO);
	}
	
	public void excluirItemPedidoVendaDTO(Long idPedidoVenda, Long idItemPedidoVenda) {
		PedidoVendaDTO pedidoVendaDTO = pedidoVendaRepository.buscarPorId(idPedidoVenda);
		ItemPedidoVendaDTO itemPedidoVendaDTO = buscarItemPedidoVendaPorId(pedidoVendaDTO, idItemPedidoVenda);
		pedidoVendaDTO.removerItemPedidoVendaDTO(itemPedidoVendaDTO);
		removerRegistro(itemPedidoVendaDTO);
	}
	
	private ItemPedidoVendaDTO buscarItemPedidoVendaPorId(PedidoVendaDTO pedidoVendaDTO, Long idItemPedidoVenda) {
		Long idPedidoVenda = pedidoVendaDTO.getIdPedidoVenda();
		Optional<ItemPedidoVendaDTO> itemPedidoOptional = pedidoVendaDTO.getItens().stream()
				                                          .filter(itemPedido -> itemPedido.getIdItemPedidoVenda().equals(idItemPedidoVenda)).findFirst();
		return itemPedidoOptional.orElseThrow(() -> new BusinessException("Item com id: " + idItemPedidoVenda + " não encontrado no pedido: " + idPedidoVenda));
	}
	
	private void calcularValorPedido(PedidoVendaDTO pedidoVendaDTO) {
		BigDecimal valorTotal = BigDecimal.ZERO;
		
		List<ItemPedidoVendaDTO> itensPedidoVendaDTO = pedidoVendaDTO.getItens();
		for (ItemPedidoVendaDTO itemPedidoVendaDTO : itensPedidoVendaDTO) {
			valorTotal = valorTotal.add(itemPedidoVendaDTO.getValorProduto());
		}
		pedidoVendaDTO.setValorTotal(valorTotal);
	}
	
	@Override
	public void validarCamposObrigatorios(ItemPedidoVendaDTO itemPedidoVendaDTO) {
		
		if(Objects.isNull(itemPedidoVendaDTO)) {
			throw new BusinessException("Informe os dados");
		}
		
		ProdutoDTO produtoDTO = itemPedidoVendaDTO.getProduto();
		BigDecimal valorProduto = itemPedidoVendaDTO.getValorProduto();
		
		if(Objects.isNull(produtoDTO) || Objects.isNull(produtoDTO.getIdProduto())) {
			throw new BusinessException("Informe o idProduto no item do pedido");
		}
		
		if(Objects.isNull(valorProduto)) {
			throw new BusinessException("Informe o valor do item do pedido");
		}
	}
	
}
