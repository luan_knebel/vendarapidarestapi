package com.vendarapida.rest.api.resources;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.ClienteDTO;
import com.vendarapida.rest.api.common.resources.ClienteResource;
import com.vendarapida.rest.api.repository.ClienteRepository;

@ManagedBean
public class ClienteResourceImpl extends AbstractResource implements ClienteResource{

	@Inject
	private ClienteRepository clienteRepository;
	
	@Override
	public List<ClienteDTO> getTodosRegistros() {
		return clienteRepository.getTodosRegistros();
	}	
	
	@Override
	public ClienteDTO buscarPorId(Long idCliente){
		return clienteRepository.buscarPorId(idCliente);
	}
	
	@Override
	public Long cadastrar(ClienteDTO clienteDTO) {
		return clienteRepository.cadastrar(clienteDTO);
	}
	
	@Override
	public void atualizar(Long idCliente, ClienteDTO clienteDTO) {
		clienteRepository.atualizar(idCliente, clienteDTO);
	}
	
	@Override
	public void excluir(Long idCliente){
		clienteRepository.excluir(idCliente);
	}
	
}
