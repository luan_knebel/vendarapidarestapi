package com.vendarapida.rest.api.repository;

import java.util.Date;
import java.util.Objects;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

import com.vendarapida.rest.api.common.dto.ClienteDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;

@ManagedBean
@ApplicationScoped
public class ClienteRepository extends AbstractRepository<ClienteDTO>{
	
	public ClienteRepository() {
		cadastrar(new ClienteDTO("João", new Date()));
		cadastrar(new ClienteDTO("Maria", new Date()));
	}
	
	@Override
	public Long cadastrar(ClienteDTO clienteDTO) {
		validarCamposObrigatorios(clienteDTO);
		Long proximoId = getProximoId();
		clienteDTO.setIdCliente(proximoId);
		adicionarRegistro(clienteDTO);
		return proximoId;
	}

	@Override
	public void atualizar(Long id, ClienteDTO clienteDTO) {
		ClienteDTO clienteAtualizarDTO = buscarPorId(id);
		validarCamposObrigatorios(clienteDTO);
		clienteAtualizarDTO.setNome(clienteDTO.getNome());
		clienteAtualizarDTO.setDataNascimento(clienteDTO.getDataNascimento());
	}
	
	@Override
	public void validarCamposObrigatorios(ClienteDTO clienteDTO) {
		
		if(Objects.isNull(clienteDTO)) {
			throw new BusinessException("Informe os dados");
		}
		if(Objects.isNull(clienteDTO.getNome())) {
			throw new BusinessException("Informe o nome");
		}
		if(Objects.isNull(clienteDTO.getDataNascimento())) {
			throw new BusinessException("Informe a data de nascimento");
		}
	}
	
}
