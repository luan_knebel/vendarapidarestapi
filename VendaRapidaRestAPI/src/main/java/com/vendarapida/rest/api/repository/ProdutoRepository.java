package com.vendarapida.rest.api.repository;

import java.util.Objects;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

import com.vendarapida.rest.api.common.dto.ProdutoDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;

@ManagedBean
@ApplicationScoped
public class ProdutoRepository extends AbstractRepository<ProdutoDTO>{

	@Override
	public Long cadastrar(ProdutoDTO produtoDTO) {
		validarCamposObrigatorios(produtoDTO);
		Long proximoId = getProximoId();
		produtoDTO.setIdProduto(proximoId);
		adicionarRegistro(produtoDTO);
		return proximoId;
	}

	@Override
	public void atualizar(Long id, ProdutoDTO produtoDTO) {
		ProdutoDTO produtoAtualizarDTO = buscarPorId(id);
		validarCamposObrigatorios(produtoDTO);
		produtoAtualizarDTO.setNomeProduto(produtoDTO.getNomeProduto());
	}

	@Override
	public void validarCamposObrigatorios(ProdutoDTO produtoDTO) {
		if(Objects.isNull(produtoDTO)) {
			throw new BusinessException("Informe os dados");
		}
		if(Objects.isNull(produtoDTO.getNomeProduto())) {
			throw new BusinessException("Informe o nome do produto");
		}
	}
	
}
