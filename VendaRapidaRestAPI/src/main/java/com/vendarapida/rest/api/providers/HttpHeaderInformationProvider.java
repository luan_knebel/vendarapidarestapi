package com.vendarapida.rest.api.providers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

@Provider
public class HttpHeaderInformationProvider implements ContainerResponseFilter{
	
	@Context
	private HttpServletRequest httpServletRequest;

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)throws IOException {
		int localPort = httpServletRequest.getLocalPort();
		responseContext.getHeaders().add("X-Application-Port", String.valueOf(localPort));		
	}

}
