package com.vendarapida.rest.api.resources;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.annotation.ManagedBean;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.TokenDTO;
import com.vendarapida.rest.api.common.dto.UsuarioDTO;
import com.vendarapida.rest.api.common.resources.UsuarioResource;
import com.vendarapida.rest.api.hateoas.UsuarioHateoas;
import com.vendarapida.rest.api.jwt.JWTUtil;
import com.vendarapida.rest.api.jwt.LoginContext;
import com.vendarapida.rest.api.repository.UsuarioRepository;

@ManagedBean
public class UsuarioResourceImpl extends AbstractResource implements UsuarioResource{
	
	@Inject
	private UsuarioHateoas usuarioHateoas;
	@Inject
	private UsuarioRepository usuarioRepository;

	@Override
	public List<UsuarioDTO> getTodosRegistros() {
		List<UsuarioDTO> listaUsuarioDTO = usuarioRepository.getTodosRegistros();
		criarHateoas(listaUsuarioDTO);
		return listaUsuarioDTO;
	}
	
	@Override
	public UsuarioDTO buscarPorId(Long idUsuario) {
		UsuarioDTO usuarioDTO = usuarioRepository.buscarPorId(idUsuario);
		criarHateoas(usuarioDTO);
		return usuarioDTO;
	}
	
	@Override
	public Long cadastrar(UsuarioDTO usuarioDTO) {
		return usuarioRepository.cadastrar(usuarioDTO);
	}
	
	@Override
	public void atualizar(Long idUsuario, UsuarioDTO usuarioDTO) {
		usuarioRepository.atualizar(idUsuario, usuarioDTO);
	}
	
	@Override
	public void excluir(Long idUsuario){
		usuarioRepository.excluir(idUsuario);
	}
	
	@Override
	@PermitAll
	public TokenDTO login(UsuarioDTO usuarioDTO) {
		LoginContext loginSession = usuarioRepository.logarUsuario(usuarioDTO);
		String token = JWTUtil.buildToken(loginSession);
		return new TokenDTO(token);
	}
	
	@Override
	public void bloquearUsuario(Long idUsuario) {
		UsuarioDTO usuarioDTO = usuarioRepository.buscarPorId(idUsuario);
		usuarioDTO.setBloqueado(true);
	}
	
	@Override
	public void desbloquearUsuario(Long idUsuario) {
		UsuarioDTO usuarioDTO = usuarioRepository.buscarPorId(idUsuario);
		usuarioDTO.setBloqueado(false);
	}
	
	private void criarHateoas(Collection<UsuarioDTO> listaUsuarioDTO) {
		if(Objects.nonNull(listaUsuarioDTO)) {
			for (UsuarioDTO usuarioDTO : listaUsuarioDTO) {
				criarHateoas(usuarioDTO);
			}
		}
	}
	
	private void criarHateoas(UsuarioDTO usuarioDTO) {
		usuarioHateoas.buildHateoas(usuarioDTO, getUriInfo());
	}
	
}
