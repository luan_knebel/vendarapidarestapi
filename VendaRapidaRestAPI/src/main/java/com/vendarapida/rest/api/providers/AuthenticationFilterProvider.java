package com.vendarapida.rest.api.providers;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Objects;

import javax.annotation.Priority;
import javax.annotation.security.PermitAll;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import com.vendarapida.rest.api.jwt.ContextRequestCache;
import com.vendarapida.rest.api.jwt.JWTUtil;
import com.vendarapida.rest.api.jwt.LoginContext;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilterProvider implements ContainerRequestFilter{

	@Context
	private ResourceInfo resourceInfo;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if(isIgnoreAuthentication()) return;
		String tokenAuthorizationHeader = getTokenAuthorizationHeader(requestContext);
		validateToken(tokenAuthorizationHeader);
	}

	private String getTokenAuthorizationHeader(ContainerRequestContext requestContext) {
		String tokenHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		return Objects.toString(tokenHeader, "");
	}

	private void validateToken(String token) {
		if(invalidAuthorizationHeader(token)) {
		  	throw new NotAuthorizedException(Response.status(Status.UNAUTHORIZED));
		}
		try {
			token = prepareToken(token);
			Jws<Claims> claims = JWTUtil.validateToken(token);
			startContextRequestCache(claims);
		} catch (Exception e) {
		  	throw new NotAuthorizedException(Response.status(Status.UNAUTHORIZED));
		}
	}
	
	private void startContextRequestCache(Jws<Claims> claims) {
		LoginContext loginContext = JWTUtil.convertToken(claims, LoginContext.class);
		ContextRequestCache.get().startCache(loginContext);
	}
	
	private boolean isIgnoreAuthentication() {
		Method resourceMethod = resourceInfo.getResourceMethod();
		return resourceMethod.isAnnotationPresent(PermitAll.class);
	}
	
	private boolean invalidAuthorizationHeader(String token) {
		return !token.startsWith("Bearer ");
	}
	
	private String prepareToken(String token) {
		return token.replace("Bearer ", "");
	}
}


