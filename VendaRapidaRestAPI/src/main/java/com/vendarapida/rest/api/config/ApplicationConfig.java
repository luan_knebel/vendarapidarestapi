package com.vendarapida.rest.api.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.vendarapida.rest.api.common.providers.ObjectMapperProvider;
import com.vendarapida.rest.api.providers.AuthenticationFilterProvider;
import com.vendarapida.rest.api.providers.ExceptionMapperProvider;
import com.vendarapida.rest.api.providers.HttpHeaderInformationProvider;
import com.vendarapida.rest.api.resources.ClienteResourceImpl;
import com.vendarapida.rest.api.resources.ItemPedidoVendaResourceImpl;
import com.vendarapida.rest.api.resources.PedidoVendaResourceImpl;
import com.vendarapida.rest.api.resources.ProdutoResourceImpl;
import com.vendarapida.rest.api.resources.UsuarioResourceImpl;

@ApplicationPath("api/rest/v1")
public class ApplicationConfig extends Application{
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> listaClasses = new HashSet<>();
		listaClasses.addAll(getListaResources());
		listaClasses.addAll(getListaProviders());
		return listaClasses;
	}
	
	private Set<Class<?>> getListaResources() {
		Set<Class<?>> listaResources = new HashSet<>();
		listaResources.add(UsuarioResourceImpl.class);
		listaResources.add(ClienteResourceImpl.class);
		listaResources.add(ProdutoResourceImpl.class);
		listaResources.add(PedidoVendaResourceImpl.class);
		listaResources.add(ItemPedidoVendaResourceImpl.class);
		return listaResources;
	}
	
	private Set<Class<?>> getListaProviders() {
		Set<Class<?>> listaProviders = new HashSet<>();
		listaProviders.add(ExceptionMapperProvider.class);
		listaProviders.add(AuthenticationFilterProvider.class);
		listaProviders.add(ObjectMapperProvider.class);
		listaProviders.add(HttpHeaderInformationProvider.class);
		return listaProviders;
	}
}
