package com.vendarapida.rest.api.resources;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 * 
 * @author Luan Knebel
 * @date 11/08/2021
 */
public abstract class AbstractResource {

	@Context
	private UriInfo uriInfo;
	
	public UriInfo getUriInfo() {
		return uriInfo;
	}
}
