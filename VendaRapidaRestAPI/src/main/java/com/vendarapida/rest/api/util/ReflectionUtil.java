package com.vendarapida.rest.api.util;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 
 * @author Luan Knebel
 * @date 11/08/2021
 */
public class ReflectionUtil {
	
	public static List<Parameter> getParametersByMethod(Class<?> resourceClass, String methodName) {
		try {
			Parameter[] parameters = getMethodByName(resourceClass, methodName).getParameters();
			if(Objects.nonNull(parameters)) {
				return Arrays.asList(parameters);
			}
			return Collections.emptyList();
		} catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}

	private static Method getMethodByName(Class<?> resourceClass, String methodName) {
		
		List<Method> declaredMethods = getDeclaredMethods(resourceClass);
		Optional<Method> optionalMethod = declaredMethods.stream().filter(method -> method.getName().equals(methodName)).findFirst();
		
		return optionalMethod.orElseThrow(() ->  new RuntimeException("Method " + methodName + " not found on class " + resourceClass.getName()));
	}
	
	private static List<Method> getDeclaredMethods(Class<?> resourceClass) {
		Method[] declaredMethods = resourceClass.getDeclaredMethods();
		if(Objects.nonNull(declaredMethods)) {
			return Arrays.asList(declaredMethods);
		}
		return Collections.emptyList();
	}
	
}
