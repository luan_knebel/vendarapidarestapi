package com.vendarapida.rest.api.resources;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.ProdutoDTO;
import com.vendarapida.rest.api.common.resources.ProdutoResource;
import com.vendarapida.rest.api.repository.ProdutoRepository;

@ManagedBean
public class ProdutoResourceImpl extends AbstractResource implements ProdutoResource{
	
	@Inject
	private ProdutoRepository produtoRepository;
	
	@Override
	public List<ProdutoDTO> getTodosRegistros() {
		return produtoRepository.getTodosRegistros();
	}
	
	@Override
	public ProdutoDTO buscarPorId(Long idProduto) {
		return produtoRepository.buscarPorId(idProduto);
	}
	
	@Override
	public Long cadastrar(ProdutoDTO produtoDTO) {
		return produtoRepository.cadastrar(produtoDTO);
	}
	
	@Override
	public void atualizar(Long idProduto, ProdutoDTO produtoDTO) {
		produtoRepository.atualizar(idProduto, produtoDTO);
	}
	
	@Override
	public void excluir(Long idProduto) {
		produtoRepository.excluir(idProduto);
	}
}
