package com.vendarapida.rest.api.repository;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

import com.vendarapida.rest.api.common.dto.UsuarioDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;
import com.vendarapida.rest.api.jwt.LoginContext;

@ManagedBean
@ApplicationScoped
public class UsuarioRepository extends AbstractRepository<UsuarioDTO>{
	
	
	public UsuarioRepository() {
		cadastrar(new UsuarioDTO("admin", "admin"));
	}
	@Override
	public Long cadastrar(UsuarioDTO usuarioDTO) {
		validarCamposObrigatorios(usuarioDTO);
		
		Long proximoId = getProximoId();
		usuarioDTO.setIdUsuario(proximoId);
		adicionarRegistro(usuarioDTO);
		return proximoId;
	}

	@Override
	public void atualizar(Long id, UsuarioDTO usuarioDTO) {
		UsuarioDTO usuarioAtualizarDTO = buscarPorId(id);
		validarCamposObrigatorios(usuarioDTO);
		usuarioAtualizarDTO.setLogin(usuarioDTO.getLogin());
		usuarioAtualizarDTO.setSenha(usuarioDTO.getSenha());
	}
	
	public LoginContext logarUsuario(UsuarioDTO usuarioDTO) {
		validarCamposObrigatorios(usuarioDTO);
		Optional<UsuarioDTO> usuarioEncontrado = getTodosRegistros().stream().filter(usuario -> isUsuarioESenhaValidos(usuario, usuarioDTO)).findFirst();
		UsuarioDTO usuarioLoginDTO = usuarioEncontrado.orElseThrow(() -> new BusinessException("Nenhum usuário encontrado com o login e senha informado."));
		return new LoginContext(usuarioLoginDTO.getIdUsuario(), usuarioLoginDTO.getLogin());
	}
	
	private boolean isUsuarioESenhaValidos(UsuarioDTO usuario, UsuarioDTO usuarioLogin) {
		return usuario.getLogin().equals(usuarioLogin.getLogin()) && usuario.getSenha().equals(usuarioLogin.getSenha());
	}
	
	@Override
	public void validarCamposObrigatorios(UsuarioDTO usuarioDTO) {
		
		if(Objects.isNull(usuarioDTO)) {
			throw new BusinessException("Informe os dados");
		}
		if(Objects.isNull(usuarioDTO.getLogin()) || usuarioDTO.getLogin().isEmpty()) {
			throw new BusinessException("Informe o login");
		}
		if(Objects.isNull(usuarioDTO.getSenha()) || usuarioDTO.getSenha().isEmpty()) {
			throw new BusinessException("Informe a senha");
		}
	}
	
}
