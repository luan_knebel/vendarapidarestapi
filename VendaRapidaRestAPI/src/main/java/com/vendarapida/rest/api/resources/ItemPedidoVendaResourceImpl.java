package com.vendarapida.rest.api.resources;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.ItemPedidoVendaDTO;
import com.vendarapida.rest.api.common.resources.ItemPedidoVendaResource;
import com.vendarapida.rest.api.repository.ItemPedidoVendaRepository;

@ManagedBean
public class ItemPedidoVendaResourceImpl extends AbstractResource implements ItemPedidoVendaResource{

	@Inject
	private ItemPedidoVendaRepository itemPedidoVendaRepository;
	
	@Override
	public List<ItemPedidoVendaDTO> buscarPorIdPedidoVenda(Long idPedidoVenda) {
		return itemPedidoVendaRepository.buscarPorIdPedidoVenda(idPedidoVenda);
	}
	
	@Override
	public ItemPedidoVendaDTO buscarPorId(Long idPedidoVenda, Long idItemPedidoVenda) {
		return itemPedidoVendaRepository.buscarPorIdItemPedidoVenda(idPedidoVenda, idItemPedidoVenda);
	}
	
	@Override
	public Long cadastrar(Long idPedidoVenda, ItemPedidoVendaDTO itemPedidoVendaDTO) {
		return itemPedidoVendaRepository.cadastrarItemPedidoVendaDTO(idPedidoVenda, itemPedidoVendaDTO);
	}
	
	@Override
	public void atualizar(Long idPedidoVenda, Long idItemPedidoVenda, ItemPedidoVendaDTO itemPedidoVendaDTO) {
		itemPedidoVendaRepository.atualizarItemPedidoVendaDTO(idPedidoVenda, idItemPedidoVenda, itemPedidoVendaDTO);
	}
	
	@Override
	public void excluir(Long idPedidoVenda, Long idItemPedidoVenda) {
		itemPedidoVendaRepository.excluirItemPedidoVendaDTO(idPedidoVenda, idItemPedidoVenda);
	}
	
}
