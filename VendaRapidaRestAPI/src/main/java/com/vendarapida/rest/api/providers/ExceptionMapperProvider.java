package com.vendarapida.rest.api.providers;

import java.util.Objects;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.vendarapida.rest.api.common.dto.ExceptionDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;

@Provider
public class ExceptionMapperProvider implements ExceptionMapper<Throwable>{

	@Override
	public Response toResponse(Throwable exception) {
		exception.printStackTrace();
		Status statusReponse = Status.INTERNAL_SERVER_ERROR;
		ExceptionDTO exceptionDTO = new ExceptionDTO();
		exceptionDTO.setMensagem(getMessage(exception));
		
		if(exception instanceof NotAuthorizedException) {
			statusReponse = Status.UNAUTHORIZED;
		}else if(exception instanceof BusinessException) {
			statusReponse = Status.BAD_REQUEST;
		}
		
		Response response = buildResponse(statusReponse, exceptionDTO);
		return response;
	}

	private Response buildResponse(Status statusReponse, Object entity) {
		return Response.status(statusReponse).entity(entity).type(MediaType.APPLICATION_JSON).build();
	}
	
	public static String getMessage(Throwable exception) {
		String message = exception.getMessage();
		Throwable cause = exception.getCause();
		if (Objects.isNull(message) && Objects.nonNull(cause)) {
			message = cause.getMessage();
		}
		if (Objects.isNull(message)) {
			message = exception.toString();
		}
		return message;
	}
}
