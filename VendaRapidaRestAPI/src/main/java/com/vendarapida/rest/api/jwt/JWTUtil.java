package com.vendarapida.rest.api.jwt;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtil {

	private final static String privateKey = "1e42a30e-7ab3-490a-979f-9a9bb6dcbaf0";
	
	public static <T> String buildToken(T object) {
		try {
			String payload = new ObjectMapper().writeValueAsString(object);
			Date dateExpiration = getdateExpiration();
			
	        return Jwts.builder()
	                .setSubject(payload)
	                .signWith(SignatureAlgorithm.HS512, privateKey)
	                .setExpiration(dateExpiration)
	                .compact();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
    public static synchronized Jws<Claims> validateToken(String token) throws Exception{
    	try {
    		return Jwts.parser().setSigningKey(privateKey).parseClaimsJws(token);
		} catch (Exception e) {
			throw new Exception(e);
		}
    }
    
    private static Date getdateExpiration() {
    	int horasDuracaoToken = 1;
    	LocalDateTime currentDate = LocalDateTime.now();
    	currentDate = currentDate.plusHours(horasDuracaoToken);
    	return Date.from(currentDate.atZone(ZoneId.systemDefault()).toInstant());
	}
    
    public static synchronized <T> T convertToken(Jws<Claims> claims, Class<T> clazz) {
    	try {
    		String payload = claims.getBody().getSubject();
			return new ObjectMapper().readValue(payload, clazz);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
	
}
