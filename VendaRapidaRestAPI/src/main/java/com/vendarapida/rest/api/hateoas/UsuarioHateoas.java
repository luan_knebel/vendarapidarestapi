package com.vendarapida.rest.api.hateoas;

import javax.annotation.ManagedBean;
import javax.ws.rs.core.UriInfo;

import com.vendarapida.rest.api.common.dto.UsuarioDTO;
import com.vendarapida.rest.api.common.resources.UsuarioResource;

@ManagedBean
public class UsuarioHateoas extends HateoasResolver<UsuarioResource, UsuarioDTO>{

	@Override
	public void resolveHateoas(UsuarioDTO usuarioDTO, UriInfo uriInfo) {
		criarHateoasBloquearUsuario(usuarioDTO, uriInfo);
		criarHateoasDesbloquearUsuario(usuarioDTO, uriInfo);
	}

	private void criarHateoasBloquearUsuario(UsuarioDTO usuarioDTO, UriInfo uriInfo) {
		if(!usuarioDTO.isBloqueado()) {
			buildHateoasLink(uriInfo, usuarioDTO, "bloquearUsuario", "bloquear");		
		}
	}
	
	private void criarHateoasDesbloquearUsuario(UsuarioDTO usuarioDTO, UriInfo uriInfo) {
		if(usuarioDTO.isBloqueado()) {
			buildHateoasLink(uriInfo, usuarioDTO, "desbloquearUsuario", "desbloquear");		
		}
	}
}
