package com.vendarapida.rest.api.repository;

import java.util.Objects;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.vendarapida.rest.api.common.dto.ClienteDTO;
import com.vendarapida.rest.api.common.dto.PedidoVendaDTO;
import com.vendarapida.rest.api.common.dto.UsuarioDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;

@ManagedBean
@ApplicationScoped
public class PedidoVendaRepository extends AbstractRepository<PedidoVendaDTO>{

	@Inject
	private ClienteRepository clienteRepository;
	
	@Inject
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Long cadastrar(PedidoVendaDTO pedidoVendaDTO) {
		validarCamposObrigatorios(pedidoVendaDTO);
		
		Long proximoId = getProximoId();
		Long idCliente = pedidoVendaDTO.getCliente().getIdCliente();
		Long idUsuario = pedidoVendaDTO.getUsuario().getIdUsuario();
		
		ClienteDTO clienteDTO = clienteRepository.buscarPorId(idCliente);
		UsuarioDTO usuarioDTO = usuarioRepository.buscarPorId(idUsuario);
		
		pedidoVendaDTO.setIdPedidoVenda(proximoId);
		pedidoVendaDTO.setCliente(clienteDTO);
		pedidoVendaDTO.setUsuario(usuarioDTO);
		
		adicionarRegistro(pedidoVendaDTO);
		return proximoId;
	}

	@Override
	public void atualizar(Long id, PedidoVendaDTO pedidoVendaDTO) {
		PedidoVendaDTO pedidoVendaAtualizarDTO = buscarPorId(id);
		validarCamposObrigatorios(pedidoVendaDTO);
		
		Long idCliente = pedidoVendaDTO.getCliente().getIdCliente();
		Long idUsuario = pedidoVendaDTO.getUsuario().getIdUsuario();
		
		ClienteDTO clienteDTO = clienteRepository.buscarPorId(idCliente);
		UsuarioDTO usuarioDTO = usuarioRepository.buscarPorId(idUsuario);
		pedidoVendaAtualizarDTO.setCliente(clienteDTO);
		pedidoVendaAtualizarDTO.setUsuario(usuarioDTO);
	}
	
	@Override
	public void validarCamposObrigatorios(PedidoVendaDTO pedidoVendaDTO) {
		if(Objects.isNull(pedidoVendaDTO)) {
			throw new BusinessException("Informe os dados");
		}
		
		ClienteDTO clienteDTO = pedidoVendaDTO.getCliente();
		UsuarioDTO usuarioDTO = pedidoVendaDTO.getUsuario();
		
		if(Objects.isNull(clienteDTO) || Objects.isNull(clienteDTO.getIdCliente())) {
			throw new BusinessException("Informe o idCliente do pedido");
		}
		
		if(Objects.isNull(usuarioDTO) || Objects.isNull(usuarioDTO.getIdUsuario())) {
			throw new BusinessException("Informe o idUsuario do pedido");
		}
	}
	
}
