package com.vendarapida.rest.api.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vendarapida.rest.api.common.dto.AbstractDTO;
import com.vendarapida.rest.api.exceptions.BusinessException;

/**
 * Simula um repositorio de banco de dados e memoria para testes.
 * Esta abstracao podera chamar um JPA posteriormente. 
 */
public abstract class AbstractRepository<DTO extends AbstractDTO>{
	
	private List<DTO> listaRegistros = new ArrayList<>();
	
	public DTO buscarPorId(Long id) {
		Optional<DTO> optional = listaRegistros.stream().filter(dto -> dto.getId().equals(id)).findFirst();
		return optional.orElseThrow(() -> new BusinessException("Nenhum registro encontrado com o id " + id));
	}

	public void excluir(Long id) {
		DTO dto = buscarPorId(id);
		removerRegistro(dto);
	}
	
	public List<DTO> getTodosRegistros(){
		return listaRegistros;
	}
	
	public void adicionarRegistro(DTO dto) {
		listaRegistros.add(dto);
	}
	
	public void removerRegistro(DTO dto) {
		listaRegistros.remove(dto);
	}
	
	public Long getProximoId() {
		return getTodosRegistros().stream().mapToLong(dto -> dto.getId()).max().orElse(0L) + 1L;
	}
	
	public abstract Long cadastrar(DTO dto);
	
	public abstract void atualizar(Long id, DTO dto);
	
	public abstract void validarCamposObrigatorios(DTO dto);
	
}
