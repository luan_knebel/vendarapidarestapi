package com.vendarapida.rest.api.jwt;

import java.io.Serializable;
import java.util.Date;

public class LoginContext implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long idUsuario;
	private String usuario;
	private Date dataLogin;
	
	public LoginContext() {
	}
	
	public LoginContext(Long idUsuario, String usuario) {
		this.idUsuario = idUsuario;
		this.usuario = usuario;
		this.dataLogin = new Date();
	}

	public Long getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public Date getDataLogin() {
		return dataLogin;
	}
	
	public void setDataLogin(Date dataLogin) {
		this.dataLogin = dataLogin;
	}
	
}
