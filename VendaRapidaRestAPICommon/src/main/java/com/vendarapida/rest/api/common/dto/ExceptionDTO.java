package com.vendarapida.rest.api.common.dto;

public class ExceptionDTO extends AbstractDTO{

	private String mensagem;
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public Long getId() {
		return null;
	}
}
