package com.vendarapida.rest.api.common.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vendarapida.rest.api.common.dto.ProdutoDTO;

@Path("produto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ProdutoResource {
	
	@GET
	public List<ProdutoDTO> getTodosRegistros();
	
	@GET
	@Path("{idProduto}")
	public ProdutoDTO buscarPorId(@PathParam("idProduto") Long idProduto);
	
	@POST
	public Long cadastrar(ProdutoDTO produtoDTO);
	
	@PUT
	@Path("{idProduto}")
	public void atualizar(@PathParam("idProduto") Long idProduto, ProdutoDTO produtoDTO);
	
	@DELETE
	@Path("{idProduto}")
	public void excluir(@PathParam("idProduto") Long idProduto);
}
