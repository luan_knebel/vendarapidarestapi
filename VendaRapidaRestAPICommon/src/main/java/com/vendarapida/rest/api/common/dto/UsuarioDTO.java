package com.vendarapida.rest.api.common.dto;

public class UsuarioDTO extends AbstractDTO{
	
	private Long idUsuario;
	private String login;
	private String senha;
	private boolean bloqueado;
	
	public UsuarioDTO() {
	}
	
	public UsuarioDTO(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public UsuarioDTO(String login, String senha) {
		this.login = login;
		this.senha = senha;
	}
	
	public Long getIdUsuario() {
		return idUsuario;
	}
	
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	@Override
	public Long getId() {
		return idUsuario;
	}

}
