package com.vendarapida.rest.api.common.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateSerializer extends JsonSerializer<Date>{

	private static final Locale locale = new Locale("pt", "BR");;
	private static final String datePattern = "dd/MM/yyyy HH:mm:ss";
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern, locale);
	
	@Override
	public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {
		String stringCalendar = dateFormat.format(date);
		jsonGenerator.writeString(stringCalendar);
	}

}
