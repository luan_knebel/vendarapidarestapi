package com.vendarapida.rest.api.common.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vendarapida.rest.api.common.dto.ItemPedidoVendaDTO;

@Path("pedidovenda/{idPedidoVenda}/itempedidovenda")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ItemPedidoVendaResource {

	@GET
	public List<ItemPedidoVendaDTO> buscarPorIdPedidoVenda(@PathParam("idPedidoVenda") Long idPedidoVenda);
	
	@GET
	@Path("{idItemPedidoVenda}")
	public ItemPedidoVendaDTO buscarPorId(@PathParam("idPedidoVenda") Long idPedidoVenda, @PathParam("idItemPedidoVenda") Long idItemPedidoVenda);
	
	@POST
	public Long cadastrar(@PathParam("idPedidoVenda") Long idPedidoVenda, ItemPedidoVendaDTO itemPedidoVendaDTO);
	
	@PUT
	@Path("{idItemPedidoVenda}")
	public void atualizar(@PathParam("idPedidoVenda") Long idPedidoVenda, @PathParam("idItemPedidoVenda") Long idItemPedidoVenda, ItemPedidoVendaDTO itemPedidoVendaDTO);
	
	@DELETE
	@Path("{idItemPedidoVenda}")
	public void excluir(@PathParam("idPedidoVenda") Long idPedidoVenda,  @PathParam("idItemPedidoVenda") Long idItemPedidoVenda);
}
