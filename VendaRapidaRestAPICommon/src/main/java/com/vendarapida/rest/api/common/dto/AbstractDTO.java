package com.vendarapida.rest.api.common.dto;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.ws.rs.core.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class AbstractDTO {
	
	@JsonIgnore
	public abstract Long getId();
	
	private Set<LinkDTO> links = new HashSet<>();

	public void addLink(Link link) {
		links.add(new LinkDTO(link));
	}
	
	public void clearLinks() {
		links.clear();
	}
	
	public Set<LinkDTO> getLinks() {
		if(links.isEmpty()) return null;
		return links;
	}
	
	@JsonIgnore
	public boolean isNew() {
		return Objects.isNull(getId());
	}
}
