package com.vendarapida.rest.api.common.resources;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vendarapida.rest.api.common.dto.TokenDTO;
import com.vendarapida.rest.api.common.dto.UsuarioDTO;

@Path("usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface UsuarioResource {

	@GET
	public List<UsuarioDTO> getTodosRegistros();
	
	@GET
	@Path("{idUsuario}")
	public UsuarioDTO buscarPorId(@PathParam("idUsuario") Long idUsuario);
	
	@POST
	@PermitAll
	public Long cadastrar(UsuarioDTO usuarioDTO);
	
	@PUT
	@Path("{idUsuario}")
	public void atualizar(@PathParam("idUsuario") Long idUsuario, UsuarioDTO usuarioDTO);
	
	@DELETE
	@Path("{idUsuario}")
	public void excluir(@PathParam("idUsuario") Long idUsuario);
	
	@POST
	@PermitAll
	@Path("login")
	public TokenDTO login(UsuarioDTO usuarioDTO);
	
	@GET
	@Path("bloquear/{idUsuario}")
	public void bloquearUsuario(@PathParam("idUsuario") Long idUsuario);
	
	@GET
	@Path("desbloquear/{idUsuario}")
	public void desbloquearUsuario(@PathParam("idUsuario") Long idUsuario);
	

}
