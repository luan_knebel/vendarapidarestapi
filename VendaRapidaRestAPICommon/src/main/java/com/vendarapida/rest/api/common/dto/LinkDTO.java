package com.vendarapida.rest.api.common.dto;

import java.net.URI;

import javax.ws.rs.core.Link;

public class LinkDTO{
	
	private String rel;
	private URI href;
	
	public LinkDTO() {
	}
	
	public LinkDTO(Link link) {
		this.rel = link.getRel();
		this.href = link.getUri();
	}
	
	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public URI getHref() {
		return href;
	}

	public void setHref(URI href) {
		this.href = href;
	}
}
