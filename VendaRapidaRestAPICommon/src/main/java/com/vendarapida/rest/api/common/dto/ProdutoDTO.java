package com.vendarapida.rest.api.common.dto;

public class ProdutoDTO extends AbstractDTO{

	private Long idProduto;
	private String nomeProduto;
	
	public ProdutoDTO() {
	}
	
	public ProdutoDTO(Long idProduto) {
		this.idProduto = idProduto;
	}

	public ProdutoDTO(Long idProduto, String nomeProduto) {
		this.idProduto = idProduto;
		this.nomeProduto = nomeProduto;
	}
	
	public Long getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	@Override
	public Long getId() {
		return idProduto;
	}
	
	
}
