package com.vendarapida.rest.api.common.dto;

import java.util.Date;

public class ClienteDTO extends AbstractDTO{
	
	private Long idCliente;
	private String nome;
	private Date dataNascimento;
	
	public ClienteDTO() {
	}
	
	public ClienteDTO(Long idCliente) {
		this.idCliente = idCliente;
	}

	public ClienteDTO(String nome, Date dataNascimento) {
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Long getId() {
		return idCliente;
	}
	
}
