package com.vendarapida.rest.api.common.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class DateDeserializer extends JsonDeserializer<Date> {

	private static final Locale locale = new Locale("pt", "BR");;
	private static final String datePattern = "dd/MM/yyyy HH:mm:ss";
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern, locale);

	@Override
	public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
		String date = jsonParser.getText();
		try {
			return dateFormat.parse(date);
		} catch (Exception e) {
			throw new RuntimeException("Formado de data inválido para o valor: " + date + ", utilize o formato " + datePattern, e);
		}
	}

}
