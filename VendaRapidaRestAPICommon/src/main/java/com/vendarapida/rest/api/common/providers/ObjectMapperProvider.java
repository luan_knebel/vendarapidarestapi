package com.vendarapida.rest.api.common.providers;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vendarapida.rest.api.common.json.ObjectMapperFactory;

@Provider
public class ObjectMapperProvider implements ContextResolver<ObjectMapper>{

	@Override
	public ObjectMapper getContext(Class<?> type) {
		return ObjectMapperFactory.getObjectMapper();
	}
	
}
