package com.vendarapida.rest.api.common.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vendarapida.rest.api.common.dto.ClienteDTO;

@Path("cliente")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ClienteResource {

	@GET
	public List<ClienteDTO> getTodosRegistros();
	
	@GET
	@Path("{idCliente}")
	public ClienteDTO buscarPorId(@PathParam("idCliente") Long idCliente) ;
	
	@POST
	public Long cadastrar(ClienteDTO clienteDTO);
	
	@PUT
	@Path("{idCliente}")
	public void atualizar(@PathParam("idCliente") Long idCliente, ClienteDTO clienteDTO);
	
	@DELETE
	@Path("{idCliente}")
	public void excluir(@PathParam("idCliente") Long idCliente);
}
