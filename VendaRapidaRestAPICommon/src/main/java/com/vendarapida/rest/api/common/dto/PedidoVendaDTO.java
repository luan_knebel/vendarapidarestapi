package com.vendarapida.rest.api.common.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PedidoVendaDTO extends AbstractDTO{
	
	private Long idPedidoVenda;
	private ClienteDTO cliente;
	private UsuarioDTO usuario;
	private BigDecimal valorTotal;
	private List<ItemPedidoVendaDTO> itens = new ArrayList<>();
	
	public PedidoVendaDTO() {
	}
	
	public PedidoVendaDTO(ClienteDTO cliente, UsuarioDTO usuario) {
		this.cliente = cliente;
		this.usuario = usuario;
	}

	public Long getIdPedidoVenda() {
		return idPedidoVenda;
	}
	
	public void setIdPedidoVenda(Long idPedidoVenda) {
		this.idPedidoVenda = idPedidoVenda;
	}
	
	public ClienteDTO getCliente() {
		return cliente;
	}
	
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public List<ItemPedidoVendaDTO> getItens() {
		return itens;
	}
	
	public void setItens(List<ItemPedidoVendaDTO> itens) {
		this.itens = itens;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
	
	public void addItemPedidoVendaDTO(ItemPedidoVendaDTO itemPedidoVendaDTO) {
		itemPedidoVendaDTO.setIdPedidoVenda(idPedidoVenda);
		itens.add(itemPedidoVendaDTO);
	}

	public void removerItemPedidoVendaDTO(ItemPedidoVendaDTO itemPedidoVendaDTO) {
		itens.remove(itemPedidoVendaDTO);
	}
	
	@Override
	public Long getId() {
		return idPedidoVenda;
	}
	
}
