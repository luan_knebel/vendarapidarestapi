package com.vendarapida.rest.api.common.dto;

import java.math.BigDecimal;

public class ItemPedidoVendaDTO extends AbstractDTO{

	private Long idItemPedidoVenda;
	private Long idPedidoVenda;
	private ProdutoDTO produto;
	private BigDecimal valorProduto;
	
	public ItemPedidoVendaDTO() {
	}
	
	public ItemPedidoVendaDTO(ProdutoDTO produto, BigDecimal valorProduto) {
		this.produto = produto;
		this.valorProduto = valorProduto;
	}
	
	public Long getIdItemPedidoVenda() {
		return idItemPedidoVenda;
	}
	
	public void setIdItemPedidoVenda(Long idItemPedidoVenda) {
		this.idItemPedidoVenda = idItemPedidoVenda;
	}
	
	public Long getIdPedidoVenda() {
		return idPedidoVenda;
	}

	public void setIdPedidoVenda(Long idPedidoVenda) {
		this.idPedidoVenda = idPedidoVenda;
	}

	public ProdutoDTO getProduto() {
		return produto;
	}
	
	public void setProduto(ProdutoDTO produto) {
		this.produto = produto;
	}
	
	public BigDecimal getValorProduto() {
		return valorProduto;
	}
	
	public void setValorProduto(BigDecimal valorProduto) {
		this.valorProduto = valorProduto;
	}

	@Override
	public Long getId() {
		return idItemPedidoVenda;
	}
	
}
