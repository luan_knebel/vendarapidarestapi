# README #

 Este repositório contém os projetos e conteúdos do curso de RESTful com Java EE.  
 Versão: 0.0.1-SNAPSHOT

### Configurações do Ambiente de Desenvolvimento ###

* Ferramentas necessárias:  
    * Java JDK 11   
    * [Eclipse IDE](https://www.eclipse.org/downloads/) version >= 2018‑12  
    * [WildFly](http://wildfly.org/downloads/) version >= 20.0.1.Final  
  
* Configurações:
    * Configurar o Eclipse IDE para rodar as aplicações com JKD em Window -> Preferences -> Java -> Installed JREs.  
    * Instalar o Plugin da Jboss Community para WildFly no Eclipse IDE.   
    * Configurar o servidor WildFly no Plugin Jboss. 
    * Importar os projetos via Maven em File -> Import -> Maven -> Existing Maven Projects.  
* Dependências do Projeto:  
    * javaee-api : version 8  
    * jsonwebtoken : version 0.9.1  
    * jackson-databind : version 2.9.6      
    * primefaces : version 6.0  
    * all-themes : version 1.0.10  
    * resteasy-client : version 3.6.3.Final  
    
* Instruções de Deploy:
    * Realizar o deploy das seguintes aplicações:
    * VendaRapidaRestAPI http://localhost:{porta}/VendaRapidaRestAPI  
    * VendaRapidaWeb http://localhost:{porta}/VendaRapidaWeb  

### Autores ###
* Desenvolvedor e Arquiteto Java EE: Luan Felipe Knebel.