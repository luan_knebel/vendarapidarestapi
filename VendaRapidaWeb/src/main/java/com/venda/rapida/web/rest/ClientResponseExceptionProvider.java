package com.venda.rapida.web.rest;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.ext.Provider;

import com.vendarapida.rest.api.common.dto.ExceptionDTO;
import com.vendarapida.rest.api.common.json.ObjectMapperFactory;

@Provider
public class ClientResponseExceptionProvider implements ClientResponseFilter{

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		
		int statusCode = responseContext.getStatus();
		Family familyResponse = Status.Family.familyOf(statusCode);
		boolean isBadRequest = Status.BAD_REQUEST.getStatusCode() == statusCode;
		boolean isClientError = familyResponse.equals(Family.CLIENT_ERROR);
		boolean isServerError = familyResponse.equals(Family.SERVER_ERROR);
		
		if(isBadRequest) {
			ExceptionDTO exceptionDTO = getExceptionDTO(responseContext);
			throw new RuntimeException(exceptionDTO.getMensagem());
		}
		if(isServerError || isClientError) {
			String formatedMessateException = getFormatedMessateException(responseContext);
			throw new RuntimeException(formatedMessateException);
		}
	}
	
	private ExceptionDTO getExceptionDTO(ClientResponseContext responseContext) {
		if(responseContext.hasEntity()) {
			return convertResponseToExceptionDTO(responseContext);
		}else {
			String formatedMessateException = getFormatedMessateException(responseContext);
			throw new RuntimeException(formatedMessateException);
		}
	}

	private ExceptionDTO convertResponseToExceptionDTO(ClientResponseContext responseContext) {
		try {
			InputStream entityStream = responseContext.getEntityStream();
			return ObjectMapperFactory.getObjectMapper().readValue(entityStream, ExceptionDTO.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private String getFormatedMessateException(ClientResponseContext responseContext) {
		int statusCode = responseContext.getStatus();
		Status status = Status.fromStatusCode(statusCode);
		return new StringBuilder()
				  .append("Ocorreu um erro na requisicao REST: ")
				  .append(statusCode)
				  .append(" - ")
				  .append(status.getReasonPhrase()).toString();
	}
}
