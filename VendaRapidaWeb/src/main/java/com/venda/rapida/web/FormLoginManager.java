package com.venda.rapida.web;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.venda.rapida.web.rest.ClientRestProxyService;
import com.vendarapida.rest.api.common.dto.TokenDTO;
import com.vendarapida.rest.api.common.dto.UsuarioDTO;
import com.vendarapida.rest.api.common.resources.UsuarioResource;

@Named
@RequestScoped
public class FormLoginManager extends AbstractFormManager{

	private static final long serialVersionUID = 1L;
	
	private UsuarioDTO usuario = new UsuarioDTO();
	
	public void efetuarLogin() throws Exception {
		try {
			TokenDTO tokenDTO = getUsuarioResource().login(usuario);
			criarSessaoLoginUsuario(tokenDTO);	
		} catch (Exception e) {
			addFacesMessage(e);
		}
	}
	
	public UsuarioResource getUsuarioResource() {
		return new ClientRestProxyService().createProxy(UsuarioResource.class);
	}
	
	private void criarSessaoLoginUsuario(TokenDTO tokenDTO) throws Exception {
		HttpSession httpSession = getHttpSession();
		httpSession.setAttribute("token", tokenDTO.getToken());
		getExternalContext().redirect("index.xhtml");
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

}
