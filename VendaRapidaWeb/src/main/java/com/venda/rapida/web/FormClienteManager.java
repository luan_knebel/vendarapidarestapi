package com.venda.rapida.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.venda.rapida.web.rest.ClientRestProxyService;
import com.vendarapida.rest.api.common.dto.ClienteDTO;
import com.vendarapida.rest.api.common.resources.ClienteResource;

@Named
@ViewScoped
public class FormClienteManager extends AbstractFormManager {

	private static final long serialVersionUID = 1L;

	private List<ClienteDTO> listaClientes = new ArrayList<>();
	private ClienteDTO clienteDTO = new ClienteDTO();

	@PostConstruct
	public void postConstruct() {
		buscarClientes();
	}

	private void buscarClientes() {
		listaClientes = getClienteResource().getTodosRegistros();
	}

	public void novoCliente() {
		clienteDTO = new ClienteDTO();
	}

	public void salvarCliente() {
		try {
			if (clienteDTO.isNew()) {
				getClienteResource().cadastrar(clienteDTO);
			} else {
				getClienteResource().atualizar(clienteDTO.getIdCliente(), clienteDTO);
			}
			addFacesMessage("Cliente salvo!");
		} catch (Exception e) {
			addFacesMessage(e);
		} finally {
			buscarClientes();
		}
	}

	public void excluirCliente() {
		try {
			Long idCliente = clienteDTO.getIdCliente();
			if (Objects.isNull(idCliente)) {
				addFacesMessage("Selecione um cliente para excluir");
				return;
			}
			getClienteResource().excluir(idCliente);
			novoCliente();
			addFacesMessage("Cliente excluído");
		} catch (Exception e) {
			addFacesMessage(e);
		} finally {
			buscarClientes();
		}
	}

	private ClienteResource getClienteResource() {
		ClienteResource clienteResource = new ClientRestProxyService().createProxy(ClienteResource.class);
		return clienteResource;
	}

	public List<ClienteDTO> getListaClientes() {
		return listaClientes;
	}

	public ClienteDTO getClienteDTO() {
		return clienteDTO;
	}

	public void setClienteDTO(ClienteDTO clienteDTO) {
		this.clienteDTO = clienteDTO;
	}

}
