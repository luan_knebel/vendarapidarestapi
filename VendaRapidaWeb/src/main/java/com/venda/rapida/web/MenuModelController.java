package com.venda.rapida.web;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

@Named
@SessionScoped
public class MenuModelController extends AbstractFormManager{
	
	private static final long serialVersionUID = 1L;
	
	private MenuModel menuModel;
	
	@PostConstruct
	public void postConstruct() {
		menuModel = new DefaultMenuModel();
        menuModel.addElement(criarMenuCadastro());
        menuModel.addElement(criarMenuMovimentos());
        menuModel.addElement(criarMenuLogout());
	}
	
	private DefaultMenuItem criarMenuLogout() {
		DefaultMenuItem menuLogout = new DefaultMenuItem("Logout");
		menuLogout.setCommand("#{menuModelController.efetuarLogout()}");
		return menuLogout;
	}
	
	private DefaultSubMenu criarMenuCadastro() {
        DefaultSubMenu cadastros = new DefaultSubMenu("Cadastros");
        cadastros.addElement(new DefaultMenuItem("Cliente", null, "/cliente.xhtml"));
        cadastros.addElement(new DefaultMenuItem("Produtos", null, "/produto.xhtml"));
        cadastros.addElement(new DefaultMenuItem("Usuários", null, "/usuario.xhtml"));
        return cadastros;
	}
	
	private DefaultSubMenu criarMenuMovimentos() {
        DefaultSubMenu movimentos = new DefaultSubMenu("Movimentos");
        movimentos.addElement(new DefaultMenuItem("Pedidos de Venda", null, "/pedidovenda.xhtml"));
        return movimentos;
	}

	public void efetuarLogout() throws Exception {
		getHttpSession().invalidate();
		getExternalContext().redirect("index.xhtml");
	}
	
	public MenuModel getMenuModel() {
		return menuModel;
	}
	
}
