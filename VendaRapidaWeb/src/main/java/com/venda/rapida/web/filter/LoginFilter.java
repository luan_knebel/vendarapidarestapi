package com.venda.rapida.web.filter;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter{
	
	private final Logger logger = Logger.getLogger(getClass().getName());
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		
		boolean isLoginPage = isLoginPageRequest(httpServletRequest);
		boolean isUserLogged = isUserLogged(httpServletRequest);
		
		if(!isUserLogged && !isLoginPage) {
			redirectToLogin(httpServletRequest, httpServletResponse);
			return;
		}
		if (isLoginPage && isUserLogged) {
			redirectToIndex(httpServletRequest, httpServletResponse);
			return;
		}
		
		chain.doFilter(request, response);
	}
	
	private boolean isLoginPageRequest(HttpServletRequest httpServletRequest) {
		String requestURI = httpServletRequest.getRequestURI();
		return (requestURI.endsWith("VendaRapidaWeb") ||
				requestURI.endsWith("VendaRapidaWeb/") ||
		        requestURI.endsWith("/login.xhtml"));
	}
	
	/**
	 * Quando a sessão possui o atributo token é porque está logado.
	 */
	private boolean isUserLogged(HttpServletRequest httpServletRequest) {
		HttpSession httpSession = httpServletRequest.getSession(true);
		return Objects.nonNull(httpSession.getAttribute("token"));
	}
	
	public void redirectToLogin(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
		String contextPath = getContextPath(httpServletRequest);
		httpServletResponse.sendRedirect(contextPath + "/login.xhtml");
	}
	
	public void redirectToIndex(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
		String contextPath = getContextPath(httpServletRequest);
		httpServletResponse.sendRedirect(contextPath + "/index.xhtml");
	}
	
	private String getContextPath(HttpServletRequest httpServletRequest) {
		ServletContext servletContext = httpServletRequest.getSession().getServletContext();
		return servletContext.getContextPath();
	}
	
	@SuppressWarnings("unused")
	private void logginSession(ServletRequest request) {
		String serverName = request.getServerName();
		int localPort = request.getLocalPort();
		StringBuilder loggginBuilder = new StringBuilder();
		String serverLogInfo = loggginBuilder.append("Request on: ").append(serverName).append(":").append(localPort).toString();
		logger.info(serverLogInfo);
	}
	
	/**
	 * Existem alguns arquivos do jsf com extenção *.xhtml com nome javax.faces.resource.
	 * Estes arquivos devem ser ignorados pelo filtro.
	 */
	@SuppressWarnings("unused")
	private boolean isNonPageRequest(HttpServletRequest httpServletRequest) {
		String requestURI = httpServletRequest.getRequestURI();
		return requestURI.contains("javax.faces.resource");
	}

}
